var models = require('./models');
const passportJWT = require('passport-jwt');
const passport = require('passport');
let JwtStrategy = passportJWT.Strategy;
let jwtOptions = {};

let ExtractJwt = passportJWT.ExtractJwt;

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "Les produits laitiers sont nos amis pour la vie";

let strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    console.log('payload received', jwt_payload);
    let user = models.Users.byId(jwt_payload.id_user);
    if (user) {
        next(null, user);
    } else {
        next(null, false);
    }
  });

passport.use(strategy);

module.exports = { strategy: strategy, jwtOptions: jwtOptions, passport: passport}