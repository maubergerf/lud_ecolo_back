var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var origineRouter = require('./routes/origine');

var cors = require('cors');
/* Passport JWT configuration */
var jwtConf = require('./jwtconf');

var app = express();


var models = require('./models/');



app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());
app.use(jwtConf.passport.initialize());
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/origine', origineRouter);

module.exports = app;
