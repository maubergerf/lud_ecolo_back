'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('objets', {
      id_objet: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      indice_objet: {
        type: Sequelize.STRING
      },
      nom_objet: {
        type: Sequelize.STRING
      },
      image_objet: {
        type: Sequelize.STRING
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('objets');
  }
};