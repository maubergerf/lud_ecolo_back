'use strict';
module.exports = (sequelize, DataTypes) => {
  const MateriauxDechets = sequelize.define('materiaux_dechets', {
    id_materiau_dechet: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_dechet: DataTypes.INTEGER,
    id_materiau: DataTypes.INTEGER
  }, {
    timestamps: false,
    tableName: "materiaux_dechets"
  });
  MateriauxDechets.associate = function(models) {
    // associations can be defined here
  };
  return MateriauxDechets;
};