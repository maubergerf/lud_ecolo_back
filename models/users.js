'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    id_user: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    pseudo: {
      type: DataTypes.STRING,
      unique: true
    },
    school: DataTypes.STRING,
    class: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.STRING
  }, {
    tableName: "users",
    timestamps: false,
  });
  Users.associate = function(models) {
    Users.hasMany(models.Scores, {foreignKey: 'id_user', sourceKey: 'id_user'});
  };
  
  Users.byId = async function(id) {
    return await Users.findOne({
        attributes: [
          'id_user',
          'pseudo',
          'school',
          'class',
          'avatar'
        ],
        where: {
          id_user: id
        }
    });
  }

    Users.byPseudo = async function(pseudo) {
      return await Users.findOne({
          attributes: [
            'id_user',
            'pseudo',
            'school',
            'class',
            'password',
            'avatar'
          ],
          where: {
            pseudo: pseudo
          }
      });
    }

  return Users;
};