'use strict';
module.exports = (sequelize, DataTypes) => {
  const Dechets = sequelize.define('Dechets', {
    id_dechet: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    image_dechet: DataTypes.STRING,
    nom_dechet: DataTypes.STRING
  }, {
    tableName: "dechets",
    timestamps: false,
  });
  Dechets.associate = function(models) {
    Dechets.belongsToMany(models.Types, {
      through: 'dechets_types',
      as: 'types',
      foreignKey: 'id_dechet',
      otherKey: 'id_type',
    });

    Dechets.belongsToMany(models.Materiaux, {
      through: 'materiaux_dechets',
      as: 'materiaux',
      foreignKey: 'id_dechet',
      otherKey: 'id_materiau'
    });
  };
  return Dechets;
};