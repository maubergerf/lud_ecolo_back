'use strict';
module.exports = (sequelize, DataTypes) => {
  const Materiaux = sequelize.define('Materiaux', {
    id_materiau: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    nom_materiau: DataTypes.STRING
  }, {
    tableName: "materiaux",
    timestamps: false,
  });
  Materiaux.associate = function(models) {
    Materiaux.belongsToMany(models.Dechets, {
      through: 'materiaux_dechets',
      as: 'dechets',
      foreignKey: 'id_materiau',
      otherKey: 'id_dechet'
    });

    Materiaux.belongsToMany(models.Objets, {
      through: 'objets_materiaux',
      as: 'objets',
      foreignKey: 'id_materiau',
      otherKey: 'id_objet'
    });
  };
  return Materiaux;
};