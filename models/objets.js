'use strict';
module.exports = (sequelize, DataTypes) => {
  const Objets = sequelize.define('Objets', {
    id_objet: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    indice_objet: DataTypes.STRING,
    nom_objet: DataTypes.STRING,
    image_objet: DataTypes.STRING
  }, {
    tableName: "objets",
    timestamps: false,
  });
  Objets.associate = function(models) {
    Objets.belongsToMany(models.Materiaux, {
      through: 'objets_materiaux',
      as: 'materiaux',
      foreignKey: 'id_objet',
      otherKey: 'id_materiau'
    });
    Objets.belongsToMany(models.Types, {
      through: 'types_objets',
      as: 'types',
      foreignKey: 'id_objet',
      otherKey: 'id_type'
    });
  };
  return Objets;
};