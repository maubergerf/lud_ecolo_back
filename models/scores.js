'use strict';
module.exports = (sequelize, DataTypes) => {
  const Scores = sequelize.define('Scores', {
    id_score: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    valeur_score: DataTypes.INTEGER,
    date_score: DataTypes.DATE
  }, {
    timestamps: false,
    tableName: 'scores'
  });
  Scores.associate = function(models) {
  
  };
  return Scores;
};