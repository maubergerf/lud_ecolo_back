'use strict';
module.exports = (sequelize, DataTypes) => {
  const DechetsTypes = sequelize.define('dechets_types', {
    id_dechet_type: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_dechet: DataTypes.INTEGER,
    id_type: DataTypes.INTEGER
  }, {
    tableName: "dechets_types",
    timestamps: false,
  });
  DechetsTypes.associate = function(models) {
    // associations can be defined here
  };
  return DechetsTypes;
};