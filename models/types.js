'use strict';
module.exports = (sequelize, DataTypes) => {
  const Types = sequelize.define('Types', {
    id_type: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    nom_type: DataTypes.STRING
  }, {
    tableName: "types",
    timestamps: false,
  });
  Types.associate = function(models) {
    Types.belongsToMany(models.Dechets, {
      through: 'dechets_types',
      as: 'dechets',
      foreignKey: 'id_type',
      otherKey: 'id_dechet',
    });

    Types.belongsToMany(models.Objets, {
      through: 'types_objets',
      as: 'objets',
      foreignKey: 'id_type',
      otherKey: 'id_objet'
    });
  };
  return Types;
};