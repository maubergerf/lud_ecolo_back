'use strict';
module.exports = (sequelize, DataTypes) => {
  const ObjetsMateriaux = sequelize.define('objets_materiaux', {
    id_objet_materiau: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_objet: DataTypes.INTEGER,
    id_materiau: DataTypes.INTEGER
  }, {
    timestamps: false,
    tableName: "objets_materiaux"
  });
  ObjetsMateriaux.associate = function(models) {
    // associations can be defined here
  };
  return ObjetsMateriaux;
};