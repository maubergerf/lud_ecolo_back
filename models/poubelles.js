'use strict';
module.exports = (sequelize, DataTypes) => {
  const Poubelles = sequelize.define('Poubelles', {
    id_poubelle: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    type_poubelle: DataTypes.STRING
  }, {
    tableName: "poubelles",
    timestamps: false,
  });
  Poubelles.associate = function(models) {
    // associations can be defined here
  };
  return Poubelles;
};