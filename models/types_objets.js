'use strict';
module.exports = (sequelize, DataTypes) => {
  const TypesObjets = sequelize.define('types_objets', {
    id_type_objet: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_objet: DataTypes.INTEGER,
    id_type: DataTypes.INTEGER
  }, {
    timestamps: false,
    tableName: "types_objets"
  });
  TypesObjets.associate = function(models) {
    // associations can be defined here
  };
  return TypesObjets;
};