'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('types', 
      [
        {
          nom_type: 'Recyclable',
        },
        {
          nom_type: 'Verre',
        },
        {
          nom_type: 'Encombrant',
        },
        {
          nom_type: 'Dangereux',
        },
        {
          nom_type: 'Liquide',
        },
        {
          nom_type: 'Biodegradable',
        },
        {
          nom_type: 'Electrique/Electronique',
        },
        {
          nom_type: 'Medicament',
        }
      ], {});
    
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('types', null, {});
  }
};
