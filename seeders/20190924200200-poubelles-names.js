'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('poubelles', 
      [
        {
          type_poubelle: 'Bleu/Gris-Jaune',
        },
        {
          type_poubelle: 'Vert',
        },
        {
          type_poubelle: 'Gris-vert/Gris-mauve',
        }
      ], {});
    
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('poubelles', null, {});
  }
};
