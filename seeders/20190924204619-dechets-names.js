'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('dechets', 
    [
      {
        image_dechet: 'assets/img/box_of_tissue.svg',
        nom_dechet: 'Boîte de mouchoirs'
      },
      {
        image_dechet: 'assets/img/milk.svg',
        nom_dechet: 'Brique de lait'
      },
      {
        image_dechet: 'assets/img/orange-juice.svg',
        nom_dechet: 'Brique de jus de fruits'
      },
      {
        image_dechet: 'assets/img/can.svg',
        nom_dechet: 'Boîte de conserve'
      },
      {
        image_dechet: 'assets/img/soda_can.svg',
        nom_dechet: 'Canette'
      },
      {
        image_dechet: 'assets/img/aerosol.svg',
        nom_dechet: 'Aérosol (pchitt pchitt)'
      },
      {
        image_dechet: 'assets/img/cup.svg',
        nom_dechet: 'Gobelet'
      },
      {
        image_dechet: 'assets/img/plastic_bottle.svg',
        nom_dechet: 'Bouteille en platique'
      },
      {
        image_dechet: 'assets/img/plastic_packaging.svg',
        nom_dechet: 'Emballage plastique'
      },
      {
        image_dechet: 'assets/img/newspaper.svg',
        nom_dechet: 'Journal'
      },
      {
        image_dechet: 'assets/img/magazine.svg',
        nom_dechet: 'Magazine'
      },
      {
        image_dechet: 'assets/img/paper.svg',
        nom_dechet: 'Feuille de papier'
      },
      {
        image_dechet: 'assets/img/box_destroyed.svg',
        nom_dechet: 'Carton usagé'
      },
      {
        image_dechet: 'assets/img/jam.svg',
        nom_dechet: 'Pot de confiture'
      },
      {
        image_dechet: 'assets/img/glass_bottle.svg',
        nom_dechet: 'Bouteille en verre'
      },
      {
        image_dechet: 'assets/img/branch.svg',
        nom_dechet: 'Déchet végetal'
      },
      {
        image_dechet: 'assets/img/tire.svg',
        nom_dechet: 'Pneu'
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('dechets', null, {});
  }
};
