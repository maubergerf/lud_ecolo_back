'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('materiaux', 
    [
      {
        nom_materiau: 'Carton',
      },
      {
        nom_materiau: 'Métal',
      },
      {
        nom_materiau: 'Plastique',
      },
      {
        nom_materiau: 'Papier',
      },
      {
        nom_materiau: 'Verre',
      },
      {
        nom_materiau: 'Autres',
      },
    ], {});
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('materiaux', null, {});
  }
};
