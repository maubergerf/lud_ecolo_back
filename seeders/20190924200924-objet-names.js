'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('objets', 
    [
      {
        indice_objet: '',
        nom_objet: 'Carton',
        image_objet: 'assets/img/box.svg'
      },
      {
        indice_objet: '',
        nom_objet: 'Polaire',
        image_objet: 'assets/img/sweater.svg'
      },
      {
        indice_objet: '',
        nom_objet: 'Bac à fleur',
        image_objet: 'assets/img/flowers.svg'
      },
      {
        indice_objet: '',
        nom_objet: 'Papier toilette',
        image_objet: 'assets/img/toilet_paper.svg'
      },
      {
        indice_objet: '',
        nom_objet: 'Vélo',
        image_objet: 'assets/img/bicycle.svg'
      },
      {
        indice_objet: '',
        nom_objet: 'Pièce de voiture',
        image_objet: 'assets/img/vehicule_pieces.svg'
      },
      {
        indice_objet: '',
        nom_objet: 'Boîte d\'œufs',
        image_objet: 'assets/img/eggs.svg'
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('objets', null, {});
  }
};