var express = require('express');
var router = express.Router();
var models = require('../models');
var jwt = require('jsonwebtoken');
var jwtConf = require('../jwtconf');
var passport = require('passport');
var bcrypt = require('bcrypt');

/* GET users listing. */
router.get('/', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    console.log("Requesting user");
    let payload = jwt.verify(req.headers.authorization.split(' ')[1], jwtConf.jwtOptions.secretOrKey);
    models.Users.byId(payload.id_user)
        .then(users => {
            res.send(users);
        });
});

router.get('/scores', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    let payload = jwt.verify(req.headers.authorization.split(' ')[1], jwtConf.jwtOptions.secretOrKey);

    models.Users.findOne({where: {id_user: payload.id_user}}).then(user => {
        user.getScores().then(scores => {
            res.json(scores);
        });
    })
});

/* POST to connect */
router.post('/signin', async function(req, res, next) {
    const { pseudo, password } = req.body;

    if (pseudo && password) {
        let user = await models.Users.byPseudo(pseudo);
        if (!user) {
            res.status(401).json({error: "no user found", user});
        } else {
            bcrypt.compare(password, user.password, (err, result) => {
                if (result) {
                    let payload = { id_user: user.id_user };
                    let token = jwt.sign(payload, jwtConf.jwtOptions.secretOrKey);
                    res.json({token: token, user: user});
                } else {
                    res.status(401).json({error: "invalid password"});
                }
            });
        }
    } else {
        res.status(401).json({error: "invalid request"});
    }
});

/* POST to signup */
router.post('/signup', function(req, res, next) {
    if (req.body.password.length < 6) {
        res.status(401).json({error: 'password must be at least 6 characters'});
    } else {
        let user = models.Users.byPseudo(req.body.pseudo).then(user => {
            if (!user) {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    req.body.password = hash;
                    models.Users.create(req.body);
                    res.json({success: true});
                })
            } else {
                res.status(401).json({error: "an user with the same pseudo already exists"});
            }
        })
    }
});

router.put('/', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    let token = req.headers.authorization.split(' ')[1];
    let payload = jwt.verify(token, jwtConf.jwtOptions.secretOrKey);

    if (req.body.password.length < 6) {
        res.status(401).json({error: 'password must contain at least 6 characters'});
    } else {
        models.Users.findOne({where: {id_user: payload.id_user}}).then(user => {
            bcrypt.compare(payload.password, user.password, (err, result) => {
                if (!result) {
                    bcrypt.hash(req.body.password, 10, (err, hash) => {
                        req.body.password = hash;
                        models.Users.update(req.body, {where: {id_user: user.id_user}}).then(user => {
                            models.Users.byId(payload.id_user).then(user =>  {
                                res.json(user);
                            })
                        }).catch(err => {
                            res.status(500).json({error: err});
                        });
                    })
                } else {
                    req.body.password = user.password;
                    models.Users.update(req.body, {where: {id_user: user.id_user}}).then(user => {
                        models.Users.byId(payload.id_user).then(user =>  {
                            res.json(user);
                        })
                    }).catch(err => {
                        res.status(500).json({error: err});
                    });
                }
                
            })
            
        })
    }

    
});



module.exports = router;
