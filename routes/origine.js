var express = require('express');
var router = express.Router();
var models = require('../models');
var sequelize = require('sequelize');
var Sequelize = sequelize.Sequelize;
var passport = require('passport');
var jwt = require('jsonwebtoken');
var jwtConfig = require('../jwtconf');
const Op = sequelize.Op;

/* GET home page. */
router.get('/question', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    let badAnswers = [];
    let goodAnswer;
    let question;
    // Prend un objet aléatoire
    models.Objets.findOne({ order: Sequelize.literal('rand()')}).then(objet => {
        question = objet;
        // Trouve les matériaux associés
        objet.getMateriaux().then(materiaux => {
            // Choisit un matériau au hasard
            let materiau = materiaux[Math.floor(Math.random() * materiaux.length)];
            // Récupère un déchet au hasard pour être la bonne réponse
            materiau.getDechets().then(dechets => {
                goodAnswer = dechets[Math.floor(Math.random() * dechets.length)];

                // Récupère deux matériaux pour les fausses réponses
                models.Materiaux.findAll({
                    where: {
                        id_materiau: {
                            [Op.ne]: materiau.id_materiau
                        }
                    },
                    order: Sequelize.literal('rand()'),
                    limit: 2
                }).then(wrong_materiaux => {
                    let promises = [];

                    wrong_materiaux.forEach(async w_m => {
                        promises.push(w_m.getDechets());                    
                    });

                    Promise.all(promises).then(data => {
                        data.forEach(wrong_dechets => {
                            badAnswers.push(wrong_dechets[Math.floor(Math.random() * wrong_dechets.length)]);
                        });

                        let badAnswersObjects = [];
                        badAnswers.forEach(a => {
                            badAnswersObjects.push({
                                name: a.nom_dechet,
                                image: a.image_dechet
                            });
                        });

                        let payload = {
                            question: {
                                name: question.nom_objet,
                                image: question.image_objet,
                                indice: question.indice_objet
                            },
                            indice: question.indice,
                            goodAnswer: {
                                name: goodAnswer.nom_dechet,
                                image: goodAnswer.image_dechet,
                            },
                            badAnswers: badAnswersObjects
                        }
                        res.json(payload);
                    })
                    
                })
            });
        });
    });
});

router.post('/score', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    let payload = jwt.verify(req.headers.authorization.split(' ')[1], jwtConfig.jwtOptions.secretOrKey);
    console.log(payload);
    
    let score = models.Scores.build({
        valeur_score: req.body.score,
        date_score: sequelize.fn('NOW'),
        id_user: payload.id_user
    });

    score.save().then(() => {
        console.log("Score stored.");
        res.status(204).send();
    }).catch(error => {
        console.error("Failed to store the score in the database: " + error);
        
    });
});

module.exports = router;
